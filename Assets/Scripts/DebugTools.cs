using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DebugTools : MonoBehaviour
{

    [SerializeField]
    public bool _enabled = false;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if( Input.GetKeyDown( KeyCode.D ) ) 
        {
            _enabled = ! _enabled;
        }
    }

    public void DrawForceVector(Vector3 start, Vector3 end)
    { 
        if(_enabled)
        {
            Debug.DrawLine(start, end, Color.green, 0f, false);
            // Debug.Log("Drawing force vector from " + start + " to " + end);
        }
    }
}
