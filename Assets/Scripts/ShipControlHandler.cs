using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class ShipControlHandler : MonoBehaviour
{


    [SerializeField]
    public GameObject _rightFire = null;

    [SerializeField]
    public GameObject _leftFire = null;

    [SerializeField]
    GameObject _planet = null;

    [SerializeField]
    float _power = 3f;

    [SerializeField]
    GameObject _debugTools = null;

    Rigidbody2D _rigidbody;
    
    float _gravity = 10f;

    Vector3 _engineOffset;
    private bool _rightEngineControl;
    private bool _leftEngineControl;

    void Start()
    {
        
        _leftFire.GetComponent<Animator>().SetFloat("propulsion", 0);
        _rightFire.GetComponent<Animator>().SetFloat("propulsion", 0);
        
        _engineOffset = this.transform.TransformDirection( new Vector3(0f, -0.5f, 0));

         Debug.DrawLine(Vector3.zero, new Vector3(0, 5, 0), Color.white, 10);
    }

    void Update()
    {
    
        
        _rightEngineControl = Input.GetKey( KeyCode.RightArrow );
        _leftEngineControl = Input.GetKey( KeyCode.LeftArrow );
    
    }


    void FixedUpdate()
    {
        
        _rigidbody = GetComponent<Rigidbody2D>();
        Transform transform = GetComponent<Transform>();
        float zRotation = transform.eulerAngles.z;
        var rad = (Mathf.PI * zRotation/ 180);

        Vector2 distance = _planet.GetComponent<Rigidbody2D>().worldCenterOfMass - (Vector2) _rigidbody.worldCenterOfMass;
        Vector2 gravityDirection = distance.normalized * _gravity / distance.magnitude;
        Vector2 direction = this.transform.TransformDirection( Vector2.up); 
        
        _rigidbody.AddForce( gravityDirection );
        _debugTools.GetComponent<DebugTools>().DrawForceVector(_rigidbody.worldCenterOfMass, _rigidbody.worldCenterOfMass + gravityDirection);

        if( _rightEngineControl )
        {
            _rightFire.GetComponent<Animator>().SetFloat("propulsion", 1);
            Vector3 pos = _rightFire.transform.position;
            Vector3 force = direction * _power;           
            _rigidbody.AddForceAtPosition(force, pos);
            _debugTools.GetComponent<DebugTools>().DrawForceVector( pos, pos + force);            
        } else {
            _rightFire.GetComponent<Animator>().SetFloat("propulsion", 0);
        }
        if( _leftEngineControl )
        {   
            _leftFire.GetComponent<Animator>().SetFloat("propulsion", 1);
            Vector3 pos = _leftFire.transform.position;
            Vector3 force = direction * _power;         
            _rigidbody.AddForceAtPosition(force, pos);
            _debugTools.GetComponent<DebugTools>().DrawForceVector( pos, pos + force);
        } else 
        {
            _leftFire.GetComponent<Animator>().SetFloat("propulsion", 0);
        }

    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        
        if(collision.gameObject.CompareTag("Asteroid"))
        {
            DestroyShip();
        } else if(collision.gameObject.CompareTag("Planet"))
        {
            Vector2 collisionDir = collision.contacts[0].point - (Vector2) transform.position;
            float inlineSpeed = Vector2.Dot(collisionDir.normalized, GetComponent<Rigidbody2D>().velocity);
            Debug.Log("impact: " + inlineSpeed);
            if( inlineSpeed > 0.5f)
            {
                DestroyShip();
            }
        }
    }

    void DestroyShip()
    {
        GetComponent<PolygonCollider2D>().enabled = false;
        GetComponent<Animator>().SetBool("destroyed", true);
    }
    
}
