using System;
using UnityEngine;

public class UniverseControlHandler : MonoBehaviour
{
    // // References to scene objects
    [SerializeField] 
    private Camera mainCamera = null;
    [SerializeField] 
    private GameObject ship = null;
    [SerializeField] 
    private GameObject space = null;
    
    // The radius of a possible camera view
    private float _spaceCircleRadius = 0;

    // Original background object dimensions
    private float _backgroundOriginalSizeX = 0;
    private float _backgroundOriginalSizeY = 0;

    private float _halfScreenWidth = 0;

    float _scaledSizeX = 0;
    float _scaledSizeY = 0;
    
    void Start()
    {
        
        // Used to determine the direction of rotation
        _halfScreenWidth = Screen.width / 2f;
        
        // Original Background Sizes
        SpriteRenderer sr = space.GetComponent<SpriteRenderer>();
        var originalSize = sr.size;
        _backgroundOriginalSizeX = originalSize.x;
        _backgroundOriginalSizeY = originalSize.y;

        _scaledSizeX = _backgroundOriginalSizeX * space.transform.lossyScale.x;
        _scaledSizeY = _backgroundOriginalSizeY * space.transform.lossyScale.y;

        // Camera height equal to the orthographic size
        float orthographicSize = mainCamera.orthographicSize;
        // Camera width equals to orthographic size multiplied by aspect ratio
        float screenAspect = (float)Screen.width / (float)Screen.height;
        // The radius of the circle describing the camera
        _spaceCircleRadius = Mathf.Sqrt(orthographicSize * screenAspect * orthographicSize * screenAspect + orthographicSize * orthographicSize);

        // The final background sprite size should allow you to move by one basic background texture size in any direction + overlap the radius of the camera also in all directions
        sr.size = new Vector2(_spaceCircleRadius * 2 + _backgroundOriginalSizeX * 4, _spaceCircleRadius * 2 + _backgroundOriginalSizeY * 4);
    }

    void Update()
    {
        // Debug.Log("Distance x: " + (space.transform.position.x - ship.transform.position.x)/ _scaledSizeX + " Units, Distance y: " + (space.transform.position.y - ship.transform.position.y)/ _scaledSizeY + " Units");
       
        
        // When the background reaches a shift equal to the original size in any direction, we return it to the origin exactly in this direction
        if (space.transform.position.x - ship.transform.position.x  >= _scaledSizeX )
        {
            space.transform.Translate( -_scaledSizeX, 0, 0);
        }
        if (space.transform.position.x - ship.transform.position.x <= -_scaledSizeX )
        {
            space.transform.Translate( _scaledSizeX , 0, 0);
        }
        if (space.transform.position.y - ship.transform.position.y >= _scaledSizeY )
        {
            space.transform.Translate(0, -_scaledSizeY, 0);
        }
        if (space.transform.position.y - ship.transform.position.y <= -_scaledSizeY )
        {
            space.transform.Translate(0, _scaledSizeY, 0);
        }
    }
    
    // private void OnDrawGizmos()
    // {
    //     // Circle describing the camera
    //     UnityEditor.Handles.color = Color.yellow;
    //     UnityEditor.Handles.DrawWireDisc(Vector3.zero , Vector3.back, _spaceCircleRadius);

    //     // Direction of travel
    //     UnityEditor.Handles.color = Color.green;
    //     UnityEditor.Handles.DrawLine(Vector3.zero, _moveVector);
    // }
}
