using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoveShipSprite : StateMachineBehaviour
{
    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponent<SpriteRenderer>().enabled = false;
        animator.GetComponent<ShipControlHandler>()._rightFire.GetComponent<SpriteRenderer>().enabled = false;
        animator.GetComponent<ShipControlHandler>()._leftFire.GetComponent<SpriteRenderer>().enabled = false;
        animator.GetComponent<ShipControlHandler>().enabled = false;
    }

}
