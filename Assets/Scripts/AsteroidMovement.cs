using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AsteroidMovement : MonoBehaviour
{

    [SerializeField]
    private GameObject planet;

    [SerializeField, Range(5, 50)]
    private float _heightOrbit;

    //[SerializeField] 
    private float _frequency = 2.0f;

    //[SerializeField]
    private float _rotationFrequency = 0.5f;


    private float twoPi = Mathf.PI * 2f;

    // Start is called before the first frame update
    void Start()
    {
        _frequency = Random.Range(1f, 3f);
        _rotationFrequency = Random.Range(-2f, 2f);
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        float x = _heightOrbit * Mathf.Cos (twoPi * Time.time * _frequency/ _heightOrbit);
        float y = _heightOrbit * Mathf.Sin (twoPi * Time.time * _frequency/ _heightOrbit);
        transform.localPosition= new Vector3(x,y,0) + planet.transform.position;
        transform.Rotate(0f, 0f, 360 * _rotationFrequency * Time.deltaTime, Space.Self);
    }
}
